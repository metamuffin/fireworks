import { Color, FogExp2, PerspectiveCamera, Scene, WebGLRenderer } from "../thirdparty/three.js";
import { OrbitControls } from "../thirdparty/jsm_patched/controls/OrbitControls.js";
import { ag } from "./index.ts";
import { PostProcessingGlobals } from "./post_processing.ts";

export interface AttractorRenderGlobals {
    camera: PerspectiveCamera
    scene: Scene
    renderer: WebGLRenderer
    post_processing: PostProcessingGlobals
    controls: OrbitControls
}

export function init_render() {
    ag.ar.renderer.domElement.style.opacity = "1"

    ag.ar.camera = new PerspectiveCamera(20, window.innerWidth / window.innerHeight, 1, 50000);
    //@ts-ignore trust me
    ag.ar.camera.position.set(10, 10, 10);

    ag.ar.scene = new Scene();
    ag.ar.scene.background = new Color(0x000000);
    ag.ar.scene.fog = new FogExp2(0x000104, 0.0000675);

    //@ts-ignore trust me
    ag.ar.camera.lookAt(ag.ar.scene.position);
}
