import { Object3D } from "../thirdparty/three.js";


export class PhysicsObject {
    vx = 0
    vy = 0
    vz = 0

    ax = 0
    ay = 0
    az = 0

    air_friction = 0.02

    constructor(
        public object: Object3D,
        public active: boolean = true
    ) { }

    tick(delta: number) {
        //@ts-ignore trust me
        this.object.position.x += this.vx * delta
        //@ts-ignore trust me
        this.object.position.y += this.vy * delta
        //@ts-ignore trust me
        this.object.position.z += this.vz * delta

        this.vx += this.ax * delta
        this.vy += this.ay * delta
        this.vz += this.az * delta
        
        this.vx *= 1 - this.air_friction
        this.vy *= 1 - this.air_friction
        this.vz *= 1 - this.air_friction

    }

}