import { Color, Group, IcosahedronGeometry, Mesh, MeshBasicMaterial } from "../thirdparty/three.js";
import { ag } from "./index.ts";
import { PhysicsObject } from "./physics.ts";


const TIMING_ACCEL = 2
const TIMING_EXPLODE = 3
const TIMING_FADE = 5
const TIMING_END = 6

const GRAVITY = -0.2
const THRUST = 5
const FRAG_COUNT = 100

export class Firework {
    object = new Group()

    rocket: PhysicsObject
    fragments: PhysicsObject[] = []

    timer = 0
    exploded = false

    material: MeshBasicMaterial

    constructor() {
        const sphere = new IcosahedronGeometry(0.03, 4)
        const color = new Color();
        color.setHSL(Math.random(), 0.7, Math.random() * 0.2 + 0.05);

        this.material = new MeshBasicMaterial({ color: color });
        this.material.transparent = true

        const rocketMesh = new Mesh(sphere, this.material)
        //@ts-ignore trust me
        rocketMesh.position.x = Math.random() * 10 - 5
        //@ts-ignore trust me
        rocketMesh.position.z = Math.random() * 10 - 5
        //@ts-ignore trust me
        rocketMesh.position.y = -5
        this.rocket = new PhysicsObject(rocketMesh)
        this.rocket.ay = THRUST + GRAVITY
        this.object.add(rocketMesh)

        for (let i = 0; i < FRAG_COUNT; i++) {
            const fragMesh = new Mesh(sphere, this.material)
            this.fragments.push(new PhysicsObject(fragMesh, false))
        }

        ag.ar.scene.add(this.object)
        ag.fireworks.add(this)
    }

    tick(delta: number) {
        this.timer += delta
        if (this.timer > TIMING_ACCEL) this.rocket.ay = GRAVITY
        if (this.timer > TIMING_EXPLODE && !this.exploded) this.explode()
        if (this.timer > TIMING_FADE) this.fade(1 - Math.max(0, (this.timer - TIMING_FADE) / (TIMING_END - TIMING_FADE)))
        if (this.timer > TIMING_END) this.destroy()

        this.rocket.tick(delta)
        this.fragments.forEach(f => f.tick(delta))
    }

    fade(x: number) {
        this.material.opacity = x
    }

    explode() {
        this.exploded = true
        this.object.remove(this.rocket.object)
        this.rocket.active = false

        for (const f of this.fragments) {
            //@ts-ignore trust me
            f.object.position.copy(this.rocket.object.position)
            f.active = true

            const phi = Math.random() * Math.PI * 2
            const costheta = Math.random() * 2 - 1
            const theta = Math.acos(costheta)
            f.vx = Math.sin(theta) * Math.cos(phi)
            f.vy = Math.sin(theta) * Math.sin(phi)
            f.vz = Math.cos(theta)
            f.ay = GRAVITY

            this.object.add(f.object)
        }
    }

    destroy() {
        ag.ar.scene.remove(this.object)
        ag.fireworks.delete(this)
    }
}

