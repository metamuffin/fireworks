
import { OrbitControls } from "../thirdparty/jsm_patched/controls/OrbitControls.js"
import { ag } from "./index.ts";


export function init_controls() {
    const controls = new OrbitControls(ag.ar.camera, ag.ar.renderer.domElement);

    controls.enableDamping = true;
    controls.dampingFactor = 0.05;
    controls.autoRotate = true
    controls.autoRotateSpeed = 0.1

    controls.screenSpacePanning = true;

    controls.minDistance = 0.1;
    controls.maxDistance = 100;

    ag.ar.controls = controls
}
