import { Vector2 } from "../thirdparty/three.js";
import { EffectComposer } from "../thirdparty/jsm_patched/postprocessing/EffectComposer.js";
import { FilmPass } from "../thirdparty/jsm_patched/postprocessing/FilmPass.js";
import { RenderPass } from "../thirdparty/jsm_patched/postprocessing/RenderPass.js";
import { ShaderPass } from "../thirdparty/jsm_patched/postprocessing/ShaderPass.js";
import { UnrealBloomPass } from "../thirdparty/jsm_patched/postprocessing/UnrealBloomPass.js";
import { FocusShader } from "../thirdparty/jsm_patched/shaders/FocusShader.js";
import { ag } from "./index.ts";
import { FPassShader } from "./shader.ts";

export interface PostProcessingGlobals {
    composer: EffectComposer
    focus_pass: ShaderPass
}

export function init_post_processing() {
    const render_pass = new RenderPass(ag.ar.scene, ag.ar.camera);

    const bloom_pass = new UnrealBloomPass(new Vector2(window.innerWidth, window.innerHeight), 2, 0.4, 0.85);
    bloom_pass.threshold = 0
    bloom_pass.strength = 5
    bloom_pass.radius = 0.1

    // const film_pass = new FilmPass(0.5, 10, 1000, undefined);

    const focus_pass = new ShaderPass(FPassShader);
    ag.ar.post_processing.focus_pass = focus_pass

    ag.ar.post_processing.composer = new EffectComposer(ag.ar.renderer);

    ag.ar.post_processing.composer.addPass(render_pass);
    ag.ar.post_processing.composer.addPass(bloom_pass);
    ag.ar.post_processing.composer.addPass(focus_pass);
    // ag.ar.post_processing.composer.addPass(film_pass);
}

