/// <reference lib="dom" />

import * as THREE from '../thirdparty/three.js';
import { WebGLRenderer } from '../thirdparty/three.js';

import { init_controls } from './controls.ts';
import { Firework } from './firework.ts';
import { init_post_processing } from './post_processing.ts';
import { AttractorRenderGlobals, init_render } from './render.ts';

export interface AppGlobals {
    ar: AttractorRenderGlobals
    fireworks: Set<Firework>
}

// @ts-ignore asd
export const ag: AppGlobals = { ui: {}, ar: { post_processing: {} }, fireworks: new Set() };

const clock = new THREE.Clock();

window.onload = () => {
    init();
    init_render()
    init_post_processing()
    init_controls()
    init_fireworks()
    animate();
}

function animate() {
    requestAnimationFrame(animate);
    render();
    ag.ar.controls.update()
}

function render() {
    const delta = clock.getDelta()
    ag.fireworks.forEach(f => f.tick(delta))
    ag.ar.post_processing.composer.render(delta)
}

function init() {
    ag.ar.renderer = new WebGLRenderer();
    ag.ar.renderer.setPixelRatio(window.devicePixelRatio);
    ag.ar.renderer.setSize(window.innerWidth, window.innerHeight);
    ag.ar.renderer.autoClear = false;
    document.body.appendChild(ag.ar.renderer.domElement);
    globalThis.addEventListener('resize', on_window_resize);
}

function init_fireworks() {
    setInterval(() => {
        new Firework()
    }, 200)
}


function on_window_resize() {
    ag.ar.renderer.setSize(window.innerWidth, window.innerHeight);
    ag.ar.camera.aspect = window.innerWidth / window.innerHeight;
    ag.ar.camera.updateProjectionMatrix();
    //@ts-ignore trust me
    ag.ar.camera.lookAt(ag.ar.scene.position);
    ag.ar.post_processing.composer.setSize(window.innerWidth, window.innerHeight);
}




