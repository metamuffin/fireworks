import { ShaderMaterial } from "../thirdparty/three.js";

export const glsl = (x: unknown) => (x as string).toString();

export function point_shader_material(): ShaderMaterial {
    const material = new ShaderMaterial({

        uniforms: {
            scale: { value: 0.1 },
            time: { value: 0 }
        },
        vertexShader: glsl`
        precision mediump float;
        precision mediump int;

        uniform float scale;

        // attribute vec3 position; it's already declared somewhere else :D
        attribute vec4 color;

        varying vec3 vPosition;
        varying vec4 vColor;

        void main() {
            vPosition = position;
            vColor = color;

            vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );
            gl_PointSize = scale * ( 300.0 / - mvPosition.z );
            gl_Position = projectionMatrix * mvPosition;
        }`,
        fragmentShader: glsl`
        precision mediump float;
        precision mediump int;

        uniform float time;

        varying vec3 vPosition;
        varying vec4 vColor;

        void main() {
            if ( length( gl_PointCoord - vec2( 0.5, 0.5 ) ) > 0.475 ) discard;
            gl_FragColor = (sin(vColor * 6.29) + 1.0) * 0.5;
        }`
    });
    return material
}

export const FPassShader = {
    uniforms: {
        resolution: { value: [window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio] }
    },
    vertexShader: glsl`

    varying vec2 vUv;
    void main() {
        vUv = uv;
        gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
    }`,
    fragmentShader: glsl`
    uniform sampler2D tDiffuse;
    uniform vec2 resolution;
    varying vec2 vUv;

    float normpdf(in float x, in float sigma) {
        return 0.39894*exp(-0.5*x*x/(sigma*sigma))/sigma;
    }

    void main() {
        float dim = 1.0-max(0.0,length(vUv*2.0-1.0)-0.8);
        vec4 texel = texture2D( tDiffuse, vUv );
        
        gl_FragColor = texel * dim;
    }`
}

