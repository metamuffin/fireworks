#!/bin/python
import os

k = os.path.dirname(__file__) + "/jsm_orig"

for root, dirs, files in os.walk(k):
    for fn in files:
        source = os.path.join(root, fn)
        target = source.replace("jsm_orig", "jsm_patched")
        if source.endswith(".js"):
            with open(source, "rt", encoding="utf-8") as sf:
                os.makedirs(os.path.dirname(target), exist_ok=True)
                with open(target, "wt+", encoding="utf-8") as tf:
                    print("patch", target)
                    tf.write(sf.read().replace(
                        "from 'three'", "from '../../three.js'"))
        else:
            with open(source, "rb") as sf:
                os.makedirs(os.path.dirname(target), exist_ok=True)
                with open(target, "wb+") as tf:
                    print("copy", target)
                    tf.write(sf.read())
